function love.conf(t)
    t.window.title = "Everest.Olympus"
    t.window.width = 540
    t.window.minwidth = 540
    t.window.height = 700
    t.window.minheight = 700
    t.window.borderless = true
    t.window.resizable = true -- true causes a flickering border to appear on some machines
    t.window.vsync = 1
    t.window.highdpi = true
    t.console = false
end
